from random import choice, random, randint
import sys
class Cards:
    def __init__(self, suit, number):
        self.suit = suit
        self.number = number
    def __str__(self):
        return f"{self.suit} {self.number}"
    __repr__ = __str__


deck = []
hand = []
r = randint(0, 51)
c = randint(0, 50)
y = randint(0, 49)

for num in range(1, 13, 1):
    for suit in ["spades", "diamonds", "hearts", "clubs"]:
        card = Cards(suit, num)
        deck.append(card)
        
hand.append(deck[r]) 
del deck[r] 
hand.append(deck[c])
del deck[c]

print(hand)
while True:
    m = 0
    for p in hand:
        m += p.number
    print(m)

    if m > 21:
        print("that was a bust.")
        sys.exit()
    
    if m == 21:
        print("you won!")
    
    action = input("hit or stand?")
    if action == "hit":
        hand.append(deck[y])
        del deck[y]
        print(hand)
    elif action == "stand":
        print(hand)
        break
    else:
        print("please type hit or stand.")    


