class LinkedListNode:
    def __init__(self, value, next_node):
        self.value = value 
        self.next_node = next_node 
    def get_value(self):
        return self.value
          
    def set_value(self, set):
        self.value = set
        
    def get_next(self):
        return self.next_node
        
    def set_next(self, nod):
        self.next_node = nod

    def __str__(self):
        return f"{self.value} {self.next_node}"
    __repr__ = __str__

class LinkedList:
    def __init__(self, head_node):
        self.head_node = head_node
    def append(self, node):
        r = self.head_node
        while True:
            if r.get_next():
                r = r.get_next()
            else:
                r.set_next(node)
                return 
    def __str__(self):
        s = str(self.head_node.get_value()) + "->"
        node = self.head_node.get_next()
        while True: 
            s += str(node.get_value()) + "->"
            if node.get_next():
                node = node.get_next()
            else:
                break
        return s
    
one = LinkedListNode(4, None)
two = LinkedListNode(7, None)

print(one)
print(two)

three = LinkedList(one)
three.append(two)
print(three)