def add(a, b):
    return a + b
print(add(4, 5))

def celsiustofahrenheit(x):
    return x * 1.8 + 32

def fahrenheittocelsius(y):
    return (y - 32)/1.8

print(celsiustofahrenheit(40))
print(fahrenheittocelsius(104))

def wef(m):
    return fahrenheittocelsius(celsiustofahrenheit(m))

print(wef(476))

h = 6

while h > 0:
    print(h)
    h -= 1
    
def countdown(j):
    if j == 0:
        return
    print(j)
    countdown(j - 1)
    
countdown(6)

def countup(w):
    if w == 11:
        return 
    print(w)
    countup(w + 1)
    
countup(0)

def multiply(z, r):
    s = 0
    while z > 0:
        s += r
        z -= 1
    return s
    
print(multiply(2, 3)) 

def recur(a, p):
    if a > 0:
        return p + recur(a - 1, p)
    else:
        return 0
    
print(recur(2, 3))

def expo(m, n):
    o = 1
    while n > 0:
        o *= m 
        n -= 1
    return o
        
print(expo(4, 3))

def recur2(h, q):
    if q > 0:
        return h * recur2(h, q - 1)
    else:
        return 1
    
print(recur2(4, 3))

def factorial(g):
    j = 1
    while g > 0:
        j = g * j
        g -= 1
    return j
        
print(factorial(5))

def factorial2(v):
    if v > 0:
        return v * factorial2(v - 1)
    else:
        return 1
    
print(factorial2(5))

def fibonacci(f):
    if f >= 3:
        return fibonacci(f - 1) + fibonacci(f - 2)
    else:
        return 1
    
print(fibonacci(6))
















