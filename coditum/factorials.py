def factorial(x):
    if x == 0:
        return 1
    else:
        return x * factorial(x - 1)
    
print(factorial(5))

def add(y, d):
    if d == 1:
        return y
    else:
        return y + add(y, d - 1)

print(add(3, 5))

def exponent(c, v):
    if v == 0:
        return 1
    else:
        return c * exponent(c, v - 1)
    
print(exponent(3, 5))

def fib(a):
    

