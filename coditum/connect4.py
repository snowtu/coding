def create_board(r, c): 
    """makes board with r rows and c columns"""
    board = []
    for x in range(c):
        col = []
        for h in range(r):
            col.append(0)
        board.append(col)
    return board

def print_board(board):
    """displays current board"""
    r = len(board[0])
    c = len(board)
    s = "\n"
    for i in range(r - 1, -1, -1):
        for j in range(0, c):
            piece = board[j][i]
            if piece == 0:
                s += "."
            else:
                s += str(piece)
        s += "\n"
    print(s)  

def drop(c, turn, board):
    """uses column c, who's turn, and drops piece on the board"""
    if not 1 <= c <= 7:
        return False
    col = board[c - 1]
    for i in range(len(col)):
        if col[i] == 0:
            col[i] = turn 
            break
    else:
        print("you held the release latch to put your pieces, but it didn't go. the strong moral of the board..")
    return True

def vertical_win(player, board):
    """win for when there's 4 verticals in row"""
    row = len(board[0])
    col = len(board)
    for c in range(col):
        for r in range(row - 3):
            if board[c][r] == board[c][r + 1] == board[c][r + 2] == board[c][r + 3] == player:
                return True
    return False

def horizontal_win(player, board):
    """win for when there's 4 horizontals in row"""
    row = len(board[0])
    col = len(board)
    for r in range(row):
        for c in range(col - 3):
            if board[c][r] == board[c + 1][r] == board[c + 2][r] == board[c + 3][r] == player:
                return True
    return False

def updiag_win(player, board):
    """win for when there's 4 up diagonal in row"""
    row = len(board[0])
    col = len(board)
    for c in range(col - 3):
        for r in range(row - 3):
            if board[c][r] == board[c + 1][r + 1] == board[c + 2][r + 2] == board[c + 3][r + 3] == player:
                return True
    return False

def downdiag_win(player, board):
    """win for when there's 4 down diagonal in row"""
    row = len(board[0])
    col = len(board)
    for c in range(col - 3):
        for r in range(row - 1, row - 4, -1):
            if board[c][r] == board[c - 1][r - 1] == board[c - 2][r - 2] == board[c - 3][r - 3] == player:
                return True
    return False
            

q = create_board(6, 7)
print_board(q)
turn = 1
while True:
    b = input("pick a column to drop your piece in (1 - 7):")
    if b == "":
        continue
    change = drop(int(b), turn, q)
    print_board(q)
    if vertical_win(turn, q) or horizontal_win(turn, q) or updiag_win(turn, q) or downdiag_win(turn, q):
        print("player", turn, "wins!")
        break
    if change:
        turn = 2 if turn == 1 else 1
