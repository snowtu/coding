from random import choice, random  
streamername = choice(["Ninja", "xQc", "shroud", "dakotaz", "Daequan", "Tfue", "Seagull", "Kephrii", "Keemstar", "timthetatman"])
gamename = choice(["Fortnite", "Overwatch", "Apex Legends", "CS:GO", "Minecraft", "Rocket League", "PUBG", "Halo", "UNO"])
costumename = choice (["Hot Dog", "Minecraft", "Princess", "Fortnite", "Cake", "Rainbow"])
class ShopItem: 
    def __init__(self, cost, name, desc, message="", stock=1):
        self.cost = cost 
        self.name = name 
        self.desc = desc
        self.message = message
        self.stock = stock
    def __str__(self):
        return f"{self.name} ${self.cost}\n{self.desc}" 
    __repr__ = __str__

class Quest:
    def __init__(self, objective, reward, message):
        self.objective = objective
        self.reward = reward 
        self.message = message
        self.completed = False

class User:
    def __init__(self, money):
        self.money = money
        self.quests = []
        self.inventory = []
        self.sniperate = 0.01
    def __str__(self):
        return f"You have {self.money}" 
    def buy(self, item):
        if self.money >= item.cost:
            print(item.message)
            item.stock -= 1 
            self.money -= item.cost
            self.inventory.append(item)
            buy_messages = [
                  f"You got something, but at what cost? Now you have ${self.money}.",
                  f"Cool new gadgets! You have ${self.money} though, but it was probably worth it.",
                  f"A purchase can go a long way, but you now have ${self.money}.",
                  f"You got something cool, but why not just use this money to donate to {streamername}? You have ${self.money} left, too."]
            print(choice(buy_messages))
            if item == inksoda:
                self.sniperate += 0.025
        else:
            debt_messages = [
                  f"You went into your pocket only to find ${self.money} and a fly. How did it get there?",
                  f"You spent all that money on pants with multiple pockets earlier and now you have ${self.money} in those pants.",
                  f"Your account only has ${self.money} after you found out your brother used your money on v-bucks."]
            print(choice(debt_messages))
    def work(self):
        self.money += 200
        work_messages = [
        f"You worked at Subway and added 6 olives to each sandwich. You now have a whopping total of ${self.money}.",
        f"You washed the dishes. Just dishes. You won everything and now have ${self.money}.",
        f"You taught coding to kids and got some money for it. Those kids probably want to hack now but you got ${self.money}.",
        f"You tested water slides and had some fun while doing so. Some money slid into your wallet as well and you now have ${self.money}.",
        f"You work for {gamename} and earn some money- wait why didn't I just increase my chances there nooo well at least I have ${self.money}."]
        print(choice(work_messages))
    def check_quests(self):
        for quest in self.quests:
            if quest.objective(self) and not quest.completed:
                quest.reward(self)
                quest.completed = True
                print(f"Your chances of getting in the game is {self.sniperate*100}%.")
    def show_inventory(self):
        if self.inventory:
            print("You wear a backpack at your chair for no reason, but it includes:")
            for item in self.inventory:
                print(f"{item.name} - {item.desc}")
        else:
            print("Inside, all you find is sadness.")
            
pancake = ShopItem(40, "Pancake Batter", "The label itself even says waffles are better.")
pb = ShopItem(60, "Skippy's Smooth Peanut Butter", "Tastes great on bread unless you're allergic.")
gun = ShopItem(400, "Ol' Timey Mccree's Sixshooter", "A shiny revolver on sale with a gun license!")
costume = ShopItem(500, f"{costumename} Costume", "A costume that's great for every party.")
inksoda = ShopItem(2000, "Inky Soda", "Reminiscent of a friend.", "This is a fine drink. This somehow helps your chances of sniping. (+2.5 Chance)")

items = [pb, gun, pancake, costume, inksoda]

def check_buypb(user):
    return pb in user.inventory

def reward_buypb(user):
    user.inventory.append(pb) 
    print("You got your peanut butter with an extra container of some, what a steal!")

buypb = Quest(check_buypb, reward_buypb, "There's a two for one sale on peanut butter! Get some peanut butter!")

def check_robstop(user):
    if gun in user.inventory:
        return gun in user.inventory
    else:
        print("You showed up and screamed that you knew kung fu before charging at one of the criminals and getting shot at. You somehow dodge all the bullets before running away in fear.")
        return False

def reward_robstop(user):
    if check_robstop:    
        user.inventory.append(pb)
        print("You ran to the bank an started to fire your weapon at the criminals. You unload a clip into one before realizing that it's just a toy gun and the license was just a manual. The cops come in 15 minutes later after your standoff with the criminals and you were thanked for your troubles. You got some peanut butter and you're seen on TV. You're a hero!")
        user.sniperate += 0.02
           
robstop = Quest(check_robstop, reward_robstop, "You hear gunshots in the distance- there's a robbery going on!") 

def check_pancook(user):
    if pancake in user.inventory:
        return pancake in user.inventory
    else:
        print("You show up at your friend's house with a paper saying 'pancake'. They are not amused.")
        return False
    
def reward_pancook(user):
    if check_pancook:
        print("You showed up at your friend's house and you gave your friend the plate of pancakes. They increased your chances manually, thanks friend!")
        user.sniperate += 0.05
        
pancook = Quest(check_pancook, reward_pancook, f"Your friend is a dev for {gamename} and says he wants some pancakes to help you. Yuck.")

def check_costumeparty(user):
    if costume in user.inventory:
        return costume in user.inventory
    else:
        print("You were kicked from the party due to your lacking of a costume. A shirt with the word 'Fun' doesn't count.")
        return False
        
def reward_costumeparty(user):
    if check_costumeparty(user):
        print(f"Your {costumename} costume was seen as 'good enough' and a picture of you eating a {costumename} themed cake got viral. Maybe that helps?")
        user.sniperate += 0.03

costumeparty = Quest(check_costumeparty, reward_costumeparty, "There's a costume party down the street, maybe it'll somehow help?")

quests = [buypb, robstop, pancook, costumeparty]

print(f"Your favorite streamer/youtuber, {streamername}, just went online and is playing {gamename}. Time to snipe them!")
user = User(0)
time = 60
while time > 0:
    action = input("You think of what to do next. (Type 'work', 'quest', 'inventory', 'shop' or 'snipe'.)")
    if action == "work":
        user.work()
    elif action == "shop":
        for i, item in enumerate(items):
            print(f"{i + 1}: {item.name} (${item.cost})")
        try:
            cart = int(input("Amazon has all this? Wow. Whatever would I choose? (Type item number to buy.)"))
        except ValueError:
            cart = 0
        item = items[cart - 1]
        if item.stock > 0:
            user.buy(item)
        else:
            print("Amazon doesn't have that in stock. I thought they had everything!")
            continue
    elif action == "quest":
        if quests == []:
            print("No pings? That's rare.")
            continue   
        for q, quest in enumerate(quests):
            print(f"{q + 1}: {quest.message}")
        newquests = int(input("You check your Discord pings on what to do. (Type quest number to do the quest.)"))
        quest = quests[newquests - 1]
        if quest.completed == False:
            user.quests.append(quest)
            print("Discord server did an @everyone. What's that about?")
            quests.remove(quest)
        else:
            print("This ping has been checked, you can't uncheck a ping.")
            continue
    elif action == "inventory":
        user.show_inventory()
        continue
    elif action == "snipe":
        if random() <= user.sniperate:
            print(f"You got into their game of {gamename} it was and they notice you. 'Who is this STREAMSNIPERX guy?', says {streamername}. You proceed to do your flashy stunts and get the attention of the chat. 'STREAM SNIPE POG', spams the chat. You feel the surge of attention as you've done it. You accomplished your goal! You were then banned from {streamername}'s chat. Worth.")
            break
        else:
            print("You enter a game as you realize they are nowhere to be found as you back out.")
    elif action == "delete system32":
        print("You saw a system32 meme and thought this was a good idea. It wasn't.")
        break
    elif action == "secrets":
        print("discord, adam, katelyn")
        continue
    elif action == "discord":
        print("You bought some fancy Discord Nitro for your friend. Wumpus approves.")
        continue
    elif action == "adam":
        print("This guy helped me make this game. Thank him.")
        continue
    elif action == "katelyn":
        print("\"Why is the timer going off? Is there a bomb?\"")
    else:
        print("Stop hitting that, that's the fourth wall! Go back to the computer.")
        continue
    user.check_quests()
    time -= 1
    print(f"You have {time} minutes left.")
if time == 0:
    print(f"'I'm going offline, have a good night!' says {streamername}. You feel a crush of defeat as the screen loads, which can mean one thing: you're too late. ")
    

#trying to stream snipe a twitch streamer before they go offline