word="baguette"
guessed=[]

while True:
    letter=input("guess a letter.")
    if letter in word:
        if letter in guessed:
            print("you already guess this.")
        else:
            print("you guess a letter.")
            guessed.append(letter)
    else:
        print("you didn't guess a letter.")
    done=True
    for x in word:
        if x in guessed:
            print(x,end="")
        else:
            print("_",end="")
            done=False
    print()
    if done:
        print("you guessed the word!")
        break