turn = 1 
board = [[-1,-1,-1],
        [-1,-1,-1],
        [-1,-1,-1]]
check = False
while True: 
    print("player " + str(turn) + "'s turn")
    row=int(input("pick a row (rows 0-2)"))
    col=int(input("pick a column (column 0-2)"))
    if board[row][col] == -1:
        board[row][col] = turn
        for x in range(3):
            if (board[x][0] == turn and board [x][1] == turn and board[x][2] == turn): #horizontal -
                print("player " + str(turn) + " won!")
                check = True
            elif (board[0][x] == turn and board [1][x] == turn and board[2][x] == turn): #vertical |
                print("player " + str(turn) + " won!")
                check = True
            elif (board[0][0] == turn and board [1][1] == turn and board[2][2] == turn): #diagonal \
                print("player " + str(turn) + " won!")
                check = True
            elif (board[2][0] == turn and board [1][1] == turn and board[0][2] == turn): #other diagonal /
                print("player " + str(turn) + " won!")
                check = True
        if turn == 1:
            turn = 2
        else:
            turn = 1
    else:
        print("someone else took that")
    
    display = ""
    for y in board:
        for x in y:
            if x == -1:
                display += "_"
            elif x == 1:
                display += "o"
            elif x == 2:
                display += "x"
        display += "\n"
    print(display)
    if check:
        break           
print('the game is over')