from math import pi 
def circle(r):
    return r**2 * pi
def square(s):
    return s**2
def rectangle(l, w):
    return l * w
def trapezoid(b, b2, h):
    return 0.5 * (b + b2) * h 
def triangle(b, h):
    return 0.5 * b * h 